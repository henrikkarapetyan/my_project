<?php

namespace App\Controller;

use App\Document\Operations;
use App\Services\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Entity\Post;

/**
 * @Route("/api/post", name="post")
 */
class PostController extends Controller
{

    private $postService;

    /**
     * PostController constructor.
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @Route("/{id}",methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns post data",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Post::class, groups={"full"}))
     *     )
     * )
     */
    public function index(int $id)
    {
        $res = $this->postService->getById($id);
        return $this->json($res['message'], $res['status']);
    }

    /**
     * @Route(methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @SWG\Response(
     *     response=200,
     *     description="creating  a new post",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Post::class, groups={"full"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="text",
     *     in="query",
     *     type="string",
     *     description="The post text"
     * )
     *@SWG\Parameter(
     *     name="title",
     *     in="query",
     *     type="string",
     *     description="The post title"
     * )
     */
    public function create(Request $request)
    {

        $data = [
            'op_type' => Operations::CREATE,
            'text' => $request->query->get('text'),
            'title' => $request->query->get('title'),
        ];
        $rabbitMessage = json_encode($data);
        $this->get('old_sound_rabbit_mq.post_modification_producer')->setContentType('application/json');
        $this->get('old_sound_rabbit_mq.post_modification_producer')->publish($rabbitMessage);

//        $this->postService->create($data);
        return $this->json(['message' => 'successfully added'], 200);
    }

    /**
     * @Route("/{id}",methods={"Patch"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Psr\Cache\InvalidArgumentException
     * @SWG\Response(
     *     response=200,
     *     description="Updating existing user properties",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Post::class, groups={"full"}))
     *     )
     * )
     * @SWG\Parameter(
     *     name="text",
     *     in="query",
     *     type="string",
     *     description="The post text"
     * )
     *@SWG\Parameter(
     *     name="title",
     *     in="query",
     *     type="string",
     *     description="The post title"
     * )
     */
    public function update(Request $request, $id)
    {
        $data = [
            'op_type' => Operations::UPDATE,
            'text' => $request->query->get('text'),
            'title' => $request->query->get('title'),
            'id' => $id
        ];
        $rabbitMessage = json_encode($data);
        $this->get('old_sound_rabbit_mq.post_modification_producer')->setContentType('application/json');
        $this->get('old_sound_rabbit_mq.post_modification_producer')->publish($rabbitMessage);

//        $res = $this->postService->update($id, $data);
        return $this->json(['message' => 'successfully updated'], 200);
    }

    /**
     * @Route("/{id}",methods={"DELETE"})
     */
    public function delete($id)
    {
        $res = $this->postService->delete($id);
        return $this->json($res['message'], $res['status']);
    }
}
