<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="post")
 */
class WelcomeController extends AbstractController
{

    /**
     * @Route(methods={"GET"})
     */
    public function index()
    {

        return $this->json(['message' => 'Welcome'], 200);
    }
}