<?php


namespace App\Services;


use App\Document\Operations;
use App\Entity\Post;
use App\Repository\PostRepository;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use DateInterval;

/**
 * Class PostService
 * @package App\Services
 */
class PostService
{

    /**
     * @var PostRepository
     */
    private $postRepository;
    /**
     * @var AdapterInterface
     */
    private $cache;
    /**
     * @var string
     */
    private $item_preffix = "post_";

    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * PostService constructor.
     * @param EntityManagerInterface $entityManager
     * @param AdapterInterface $cache
     * @param DocumentManager $dm
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AdapterInterface $cache,
        DocumentManager $dm
    )
    {
        $this->postRepository = $entityManager->getRepository(Post::class);
        $this->cache = $cache;
        $this->dm = $dm;
    }

    /**
     * @param int $id
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getById(int $id)
    {
        $item = $this->getDataFromCache($id);
        if ($item->isHit()) {
            return ['message' => $item->get(), 'status' => 200];
        } else {
            $post = $this->postRepository->find($id);
            if (!empty($post)) {
                $this->addDataIntoCache($id, $post);

                return ['message' => $post, 'status' => 200];
            }
            return ['message' => 'not found', 'status' => 404];
        }
    }


    /**
     * @param $id
     * @return \Symfony\Component\Cache\CacheItem
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getDataFromCache($id)
    {
        return $this->cache->getItem($this->item_preffix . $id);
    }

    /**
     * @param $id
     * @param $data
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function addDataIntoCache($id, $data)
    {
        $item = $this->getDataFromCache($id);
        $item->set($data);
        $item->expiresAfter(new DateInterval('PT60S'));
        $this->cache->save($item);
    }

    /**
     * @param $id
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function delete($id)
    {
        $post = $this->postRepository->find($id);
        if (!empty($post)) {
            $this->postRepository->remove($post);
            $item = $this->getDataFromCache($id);
            if ($item->isHit()) {
                $this->cache->deleteItem($item->getKey());
            }
            $this->writeIntoOperationsLog($id, Operations::DELETE);

            return ['message' => 'deleted successfully', 'status' => 200];
        }
        return ['message' => 'not found', 'status' => 404];

    }

    /**
     * @param $id
     * @param $data
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function update($data)
    {
        $id = $data['id'];
        $post = $this->postRepository->find($id);
        if (!empty($post)) {
            $title = $data['title'];
            $text = $data['text'];
            $post->setTitle($title);
            $post->setText($text);
            $this->postRepository->save($post);
            $this->updateCacheItem($id, $post);
            $this->writeIntoOperationsLog($id, Operations::UPDATE);
            return ['message' => 'updated successfully', 'status' => 200];
        }
        return ['message' => 'not found', 'status' => 404];
    }

    /**
     * @param $id
     * @param Post|null $post
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function updateCacheItem($id, ?Post $post)
    {
        $item = $this->getDataFromCache($id);
        if ($item->isHit()) {
            $this->cache->deleteItem($item->getKey());
            $item->set($post);
            $item->expiresAfter(new DateInterval('PT60S'));
            $this->cache->save($item);
        }
    }

    /**
     * @param array $data
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function create(array $data)
    {
        $post = new Post();
        $post->setText($data['text']);
        $post->setTitle($data['title']);
        $this->postRepository->save($post);
        $this->writeIntoOperationsLog($post->getId(), Operations::CREATE);
    }

    /**
     * @param $id
     * @param $operation_type
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    private function writeIntoOperationsLog($id, $operation_type)
    {
        $op = new Operations();
        $op->setOperation($operation_type);
        $op->setPostId($id);
        $this->dm->persist($op);
        $this->dm->flush();
    }
}