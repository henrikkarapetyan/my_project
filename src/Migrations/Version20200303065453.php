<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200303065453 extends AbstractMigration
{

    private $_postTable = 'post';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable($this->_postTable);

        $table->addColumn('id', 'integer', [
            'autoincrement' => true,
        ]);

        $table->addColumn('title', 'string', [
            'notnull' => true,
        ]);

        $table->addColumn('text', 'string', [
            'notnull' => true
        ]);

        $table->setPrimaryKey(array('id'));

    }

    public function down(Schema $schema): void
    {
        $schema->dropTable($this->_postTable);
    }
}
