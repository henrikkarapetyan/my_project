<?php


namespace App\Rabbit;


use App\Document\Operations;
use App\Services\PostService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class PostModificationConsumer implements ConsumerInterface
{

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var PostService
     */
    private $postService;

    public function __construct(LoggerInterface $logger, PostService $postService)
    {
        $this->logger = $logger;
        $this->postService = $postService;
    }

    public function execute(AMQPMessage $msg)
    {

        $response = json_decode($msg->body, true);
        $op_type = $response['op_type'];
        $this->logger->info('xxxx '.$op_type);
        switch ($op_type) {
            case Operations::UPDATE:
                $this->postService->update($response);
                break;
            case Operations::CREATE:
                $this->postService->create($response);
                break;
            default:
                $this->logger->error('unknown command');
        }

        $this->logger->info('added into db');
    }

}