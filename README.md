# How to install project
1) go to project root directory
2) run this command ``` composer install ```
3) configure .env file set your properties for db connection and run this command for create db
    ``` php bin/console doctrine:database:create ```
4) ```php  bin/console assets:install```
5) run this command in console
```php bin/console rabbitmq:consumer post_modification -vvv```